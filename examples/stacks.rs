//! Shows stack layout
//!
//! Exit with ctrl-c

use std::io;
use termit::prelude::*;

#[async_std::main]
async fn main() -> io::Result<()> {
    env_logger::init();

    let mut termit = Terminal::try_system_default()?.into_termit::<NoAppEvent>();
    let light = false;
    let mut ui = Canvas::default().back(Color::black())
    .front(if light {Color::black()} else {Color::white()})
        .contain(
        Stack::south_east(vec![])
            .add(Canvas::new("Blue").back(Color::blue(light))
                .left(0).top(0)
                .width(10).height(5)
            )
            .add(Canvas::new( "Red\n  hit\n ctrl+c").back(Color::red(light))
                .left(0).top(0)
                .width(11).height(6)
            )
            .add(Canvas::new( "Green").back(Color::green(light))
                .left(0).top(0)
                .width(3).height(4),
            )
            .add(Canvas::new( "Cyan and many sdfs sdfsd fsdfs dfs dsdf dfs dfsdf sdsdf df sdf sdf sdfs sdf sdfsdf ").back(Color::cyan(light))
                .left(0).top(0)
                .width(15).height(10)
            )
            .add(Canvas::new( "Yellow").back(Color::yellow(light))
                .left(0).top(0)
                .width(5).height(5)
            )
            .add(Canvas::new( "Magenta").back(Color::magenta(light))
                .left(0).top(0)
                .width(2).height(2)
            ),
    );

    loop {
        termit.step(&mut (), &mut ui).await?;
    }

    //Ok(())
}
