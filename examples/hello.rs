use std::io;
use termit::prelude::*;
#[async_std::main]
async fn main() -> io::Result<()> {
    // The Termit facilitator:
    // * Use the Terminal to set up your terminal experience.
    // * Initialize Termit.
    let mut termit = Terminal::try_system_default()?.into_termit::<NoAppEvent>();

    // The TUI tree:
    // * Canvas sets the default style for the contained widget and fills space.
    // * "Hello World!" - &str/String - is a widget.
    // * We just place the text in the middle with some placement constraints.
    let mut ui = Canvas::new("Hello World!".width(6).height(2)).back(Color::blue(false));

    // Render and update and print... rinse and repeat?
    termit.step(&mut (), &mut ui).await?;
    //                 \       |
    //   the mutable    |      |
    //   application    |      |
    //   model goes here       \- The UI to show off

    Ok(())
}
