use ::termit::output::command::{CaptureMouse, EnterRawMode, TerminalCommander};
use futures_lite::StreamExt;
use log::info;
use std::io::{self, Write};
use termit::{
    output::{
        color::Color,
        command::{MoveCursorTo, TerminalRequest},
    },
    prelude::*,
    Terminal,
};

#[async_std::main]
async fn main() -> io::Result<()> {
    env_logger::init();

    let mut terminal = Terminal::try_system_default()?;

    terminal.send(EnterRawMode)?;
    terminal.send(CaptureMouse(true))?;

    let raw_mode = terminal.control.is_raw();
    let Terminal {
        control,
        input,
        output: _,
    } = terminal;
    let mut stream = terminal_input_stream::<()>(control, input, raw_mode);

    for i in 0..=255 {
        let (red, green, blue) = Color::ansi(i).to_24b();
        if i <= 16 && i % 8 == 0 || i > 16 && (i - 16) % 36 == 0 {
            print!("\x1b[0m\r\n");
        }
        print!("\x1b[48;2;{red};{green};{blue}m ");
    }
    print!("\x1b[0m\r\n\r\n");

    let mut w = std::io::stdout();

    MoveCursorTo(point(10, 10)).apply(&mut w)?;

    w.write_all(b"AAAA")?;

    w.flush()?;

    while let Some(item) = stream.next().await.transpose()? {
        info!("{item:?}");

        if matches!(
            item,
            Event::Key(KeyEvent {
                keycode: KeyCode::Char('q'),
                ..
            })
        ) {
            break;
        }
    }

    Ok(())
}
