//! This is an almost minimal hello world termit example with async.
//!
//! There is no input processing, no looping. Just print some nice screen.
use std::io;
use termit::prelude::*;
#[async_std::main]
async fn main() -> io::Result<()> {
    env_logger::init();

    // The Terminal facilitator:
    // * Use the Terminal to set up your terminal experience.
    let mut termit = Terminal::try_system_default()?
        // Notice the empty input - this emulates closed input.
        // This would by default cause termit to stop in error.
        .reading(b"".as_ref())
        .into_termit::<NoAppEvent>()
        .use_alternate_screen(true)?
        // here we ignore missing input
        .run_without_input(true);

    // The TUI tree:
    // * Canvas sets the default style for the contained widget.
    // * "Hello World!" - &str/String - is a widget.
    // * We just place the text in the middle with some placement constraints.
    let mut ui = Canvas::new("  Hi  World!".width(6).height(2)).back(Color::blue(false));

    // Render and update:
    termit.step(&mut (), &mut ui).await?;
    //                 \    |
    //   the mutable    |   |
    //   application    |   |
    // model goes here -/   |
    //                       \- termit needs to get
    //                           the terminal size

    // show it off for a while
    std::thread::sleep(std::time::Duration::from_secs(3));

    Ok(())
}
