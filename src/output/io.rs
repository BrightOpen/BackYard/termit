use futures_lite::{io as aio, ready};
use std::{io, pin::Pin, task::Poll};

pub(crate) struct CountingWrites<IO> {
    pub inner: IO,
    pub count: usize,
    pub busy: bool,
}
impl<IO> io::Write for CountingWrites<IO>
where
    IO: io::Write,
{
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match self.inner.write(buf) {
            Ok(len) => {
                self.busy = true;
                self.count += len;
                Ok(len)
            }
            Err(e) => Err(e),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        self.busy = false;
        self.inner.flush()
    }
}
impl<IO> CountingWrites<IO> {
    pub(crate) fn new(io: IO) -> Self {
        Self {
            inner: io,
            count: 0,
            busy: false,
        }
    }
}
impl<T> aio::AsyncWrite for CountingWrites<Pin<Box<T>>>
where
    T: aio::AsyncWrite,
{
    fn poll_write(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<io::Result<usize>> {
        let len = ready!(self.inner.as_mut().poll_write(cx, buf))?;
        self.count += len;
        Poll::Ready(Ok(len))
    }

    fn poll_flush(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<io::Result<()>> {
        self.inner.as_mut().poll_flush(cx)
    }

    fn poll_close(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<io::Result<()>> {
        self.inner.as_mut().poll_close(cx)
    }
}
