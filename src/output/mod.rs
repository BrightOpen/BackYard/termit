//! Everything related to ANSI output - styling, commands, colors
pub mod ansi;
pub mod color;
pub mod command;
pub(crate) mod io;
pub mod style;
