//! All things about processing input data

pub mod events;
mod merge;
mod myio;
#[cfg_attr(not(feature = "parser"), path = "empty.rs")]
mod parser;
mod refresh;
mod resizer;
mod squash;
#[cfg_attr(not(feature = "parser"), path = "stream-no-parser.rs")]
mod stream;

pub(crate) use merge::*;
pub use myio::*;
pub(crate) use refresh::*;
pub(crate) use resizer::*;
pub(crate) use squash::*;
pub use stream::*;
