use super::events::{AppEvent, Event};
use super::Resizer;
use crate::Control;
use futures_lite::io::{self};
use futures_lite::{stream, AsyncReadExt, Stream, StreamExt};

/// Terminal input stream decodes ANSI sequences and content and includes window resize events
///
/// It will not do much of resizing without a control FD/Handle that can report it.
///
/// The `raw_mode` param will treat \r \n slightly differently.
pub fn terminal_input_stream<'a, A: AppEvent>(
    control: impl Control + 'a,
    reader: impl io::AsyncRead + 'a,
    raw_mode: bool,
) -> impl Stream<Item = io::Result<Event<A>>> + 'a {
    let ansi = terminal_ansi_input_stream(reader, raw_mode);
    Resizer::new(Box::pin(ansi), Box::pin(control))
}

/// Terminal input stream without a `parser` feature simply pops bytes one by one
pub fn terminal_ansi_input_stream<'a, A: AppEvent>(
    reader: impl io::AsyncRead + 'a,
    _raw_mode: bool,
) -> impl Stream<Item = io::Result<Event<A>>> + 'a {
    Box::pin(reader)
        .bytes()
        .map(|b| b.map(|b| Event::Unknown(vec![b])))
        .chain(stream::once(Ok(Event::InputClosed)))
}
