#![cfg_attr(docsrs, feature(doc_cfg))]
#![doc = include_str!("../README.md")]

#[cfg(feature = "crossterm")]
mod crossterm;
pub mod geometry;
pub mod input;
mod mmap;
pub mod output;
mod painter;
mod screen;
pub(crate) mod sys;

#[cfg(feature = "temu")]
mod temu;
mod terminal;
mod termit;
pub mod widget;

pub use self::painter::*;
pub use self::screen::*;
pub use self::sys::Control;
#[cfg(feature = "temu")]
pub use self::temu::*;
pub use self::terminal::*;
pub use self::termit::*;

pub mod prelude {
    //! All the stuff you'll probably need when making Termit TUIs and/or widgets
    pub use crate::geometry::*;
    pub use crate::input::events::*;
    pub use crate::input::*;
    pub use crate::output::color::*;
    pub use crate::output::style::*;
    pub use crate::widget::*;
    pub use crate::*;
}
