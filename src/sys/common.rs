use crate::prelude::{point, Point};

pub fn get_terminal_size_no_sys() -> Option<Point> {
    get_terminal_size_from_env()
        .or_else(get_terminal_size_from_tput)
        .or(Some(point(80, 24)))
}
pub fn get_terminal_size_from_env() -> Option<Point> {
    std::env::var("COLUMNS")
        .ok()
        .and_then(|c| c.parse().ok())
        .and_then(|c| {
            std::env::var("LINES")
                .ok()
                .and_then(|l| l.parse().ok())
                .map(|l| point(c, l))
        })
}

/// Returns the size of the screen as determined by tput.
///
/// This alternate way of computing the size is useful
/// when in a subshell.
pub fn get_terminal_size_from_tput() -> Option<Point> {
    match (tput_value("cols"), tput_value("lines")) {
        (Some(w), Some(h)) => Some((w, h).into()),
        _ => None,
    }
}

/// execute tput with the given argument and parse
/// the output as a u16.
///
/// The arg should be "cols" or "lines"
fn tput_value(arg: &str) -> Option<u16> {
    let output = std::process::Command::new("tput").arg(arg).output().ok()?;
    let value = output
        .stdout
        .into_iter()
        .filter_map(|b| char::from(b).to_digit(10))
        .fold(0, |v, n| v * 10 + n as u16);

    if value > 0 {
        Some(value)
    } else {
        None
    }
}
