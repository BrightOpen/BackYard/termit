//! internal system specific code

mod common;
#[cfg_attr(all(unix, feature = "sys"), path = "nix/mod.rs")]
#[cfg_attr(all(windows, feature = "sys"), path = "win/mod.rs")]
mod inner;

pub use common::*;
pub use inner::*;

use crate::prelude::Point;

/// Essentail part of a terminal application - controling the tty.
///
/// You could implement your own [`Control`] for a specific system not covered here (unix/windows),
/// ... and contribute. The implementations are target specific.
///
/// If you compile without the sys feature or on an unsupported system,
/// you will get a dummy implementation when creating the default system terminal.
pub trait Control {
    /// Enable raw terminal mode to receive ANSI input
    #[must_use("Resets on drop")]
    fn enable_raw_mode(&self) -> std::io::Result<()>;
    /// Get terminal window size
    fn get_size(&self) -> std::io::Result<Point>;
    /// Check if it's a tty
    fn is_tty(&self) -> bool;
    /// Check it it's been switched to raw
    fn is_raw(&self) -> bool;
    /// Check if the control supports ANSI
    fn is_ansi(&self) -> bool;
    fn output(&self) -> &ControlOut;
    fn input(&self) -> &ControlIn;
    fn clone(&self) -> Self
    where
        Self: Sized;
    #[cfg(all(windows, feature = "sys"))]
    fn get_initial_style(&self) -> u16;
    #[cfg(all(windows, feature = "sys"))]
    fn use_alternate_screen(&self, alternate: bool) -> std::io::Result<()>;
}
impl<T> Control for &T
where
    T: Control,
    T: ?Sized,
{
    fn enable_raw_mode(&self) -> std::io::Result<()> {
        T::enable_raw_mode(self)
    }
    fn get_size(&self) -> std::io::Result<Point> {
        T::get_size(self)
    }
    fn is_tty(&self) -> bool {
        T::is_tty(self)
    }
    fn is_raw(&self) -> bool {
        T::is_raw(self)
    }
    fn is_ansi(&self) -> bool {
        T::is_ansi(self)
    }
    fn output(&self) -> &ControlOut {
        T::output(self)
    }
    fn input(&self) -> &ControlIn {
        T::input(self)
    }
    fn clone(&self) -> Self
    where
        Self: Sized,
    {
        self
    }
    #[cfg(all(windows, feature = "sys"))]
    fn get_initial_style(&self) -> u16 {
        T::get_initial_style(self)
    }
    #[cfg(all(windows, feature = "sys"))]
    fn use_alternate_screen(&self, alternate: bool) -> std::io::Result<()> {
        T::use_alternate_screen(self, alternate)
    }
}
